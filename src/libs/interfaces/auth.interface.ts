export interface IRegister {
  firstName: string;
  lastName: string;
  username: string;
  password: string;
}

export interface ILogin {
  username: string;
  pass: string;
}
