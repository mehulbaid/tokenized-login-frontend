import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { LayoutService } from './layout.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {

  layoutUrl: string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private canActivateGuard: AuthGuard,
    private layoutService: LayoutService
    ) {
      this.layoutUrl = `http://localhost:3000/img/${this.layoutService.getUserUploadedImage()}`;
    }

  onLogout() {
    localStorage.clear();
    this.canActivateGuard.canActivate(
      this.route.snapshot,
      this.router.routerState.snapshot
    );
  }

  ngOnInit(): void {}

}
