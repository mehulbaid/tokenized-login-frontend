import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LayoutService {

  constructor() { }

  getUserUploadedImage() {
    const userData: any = JSON.parse(localStorage.getItem('userData'));
    return userData.userDetail.filename;
  }
}
