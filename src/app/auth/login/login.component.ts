import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm = new FormGroup({
    username: new FormControl('', Validators.required),
    pass: new FormControl('', Validators.required)
  });

  constructor(
    private router: Router,
    private authService: AuthService,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
  }

  submit() {
    this.authService.loginUser(this.loginForm.value).subscribe((res: any) => {
      console.log(res);
      if (res.status === 'failed') {
        this.toastr.error(res.data, 'Login Error');
      } else if (res.status === 'success') {
        this.toastr.success('User Registered Successfully', 'User Login Success');
        localStorage.setItem('userData', JSON.stringify(res.data));
        this.router.navigate(['main']);
      }
    });
    console.log(this.loginForm.value)
  }

  notRegistered() {
    this.router.navigate(['auth/register']);
  }

  viewFormError(fieldName: string) {
    // tslint:disable-next-line: max-line-length
    return this.loginForm.get(fieldName).touched || this.loginForm.get(fieldName).dirty || this.loginForm.get(fieldName).hasError;
  }

}
