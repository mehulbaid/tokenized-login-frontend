import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { AuthService } from '../auth.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  uploadedFileName = {
    filename: null
  };

  registerForm = new FormGroup({
    firstName: new FormControl('', Validators.required),
    lastName: new FormControl('', Validators.required),
    phoneNumber: new FormControl('', [Validators.required, Validators.pattern(/^[6,7,8,9]{1}[0-9]{9}$/)]),
    city: new FormControl(null, Validators.required),
    username: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required)
  });

  cities = [
    { name: 'delhi', value: 'delhi' },
    { name: 'mumbai', value: 'mumbai' },
    { name: 'vadodara', value: 'vadodara' }
  ];

  constructor(
    private router: Router,
    private authService: AuthService,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
  }

  submit() {
    const registerFormValue = this.registerForm.value;
    const newFormData = Object.assign(registerFormValue, this.uploadedFileName);
    console.log(newFormData);
    this.authService.addNewUser(this.registerForm.value).subscribe((res: any) => {
      console.log(res);
      if (res.status === 'failed') {
        if (res.data.errCode === 'MongoError 11000') {
          this.toastr.error('User Already Registered, Please Login', 'User Already Registered');
          this.router.navigate(['auth/login']);
        } else {
          this.toastr.error(res.data.message, 'User Registration Failed');
        }
      } else if (res.status === 'success') {
        this.toastr.success('User Registered Successfully', 'User Registration Success');
        this.router.navigate(['auth/login']);
      }
    });
  }

  onImageChange(event: any) {
    const file = event.target.files[0];
    console.log(file);

    this.authService.uploadImage(file).subscribe((res: any) => {
      this.uploadedFileName.filename = res.filename;
    });
  }

  viewFormError(fieldName: string) {
    // tslint:disable-next-line: max-line-length
    return this.registerForm.get(fieldName).touched || this.registerForm.get(fieldName).dirty;
  }

}
