import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ILogin } from '../../libs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  headers = new HttpHeaders({
    'Content-Type': 'application/json'
  });

  constructor(private http: HttpClient) { }

  uploadImage(file: any) {
    const formData = new FormData();
    formData.append('image', file, file.name);

    return this.http.post('http://localhost:3000/uploadImage', formData);
  }

  addNewUser(body) {
    return this.http.post('http://localhost:3000/auth/register', body, { headers: this.headers });
  }

  loginUser(loginBody: ILogin) {
    return this.http.post('http://localhost:3000/auth/login', loginBody, { headers: this.headers });
  }
}
